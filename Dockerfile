FROM maven:3.6-jdk-8 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM gcr.io/distroless/java
COPY --from=build /usr/src/app/target/microservices-0.0.1-SNAPSHOT.jar /usr/app/microservices-0.0.1-SNAPSHOT.jar

EXPOSE 8761

ENTRYPOINT ["java","-jar","/usr/app/microservices-0.0.1-SNAPSHOT.jar"]